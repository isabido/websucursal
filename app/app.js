(function () {
  'use strict';

var app = angular.module("tattlerApp",
        [   "ngResource",
            "ngRoute",
            'LocalStorageModule',
            "routeStyles",
            "chieffancypants.loadingBar",
            "ngAnimate",
            "multi-select",
            "ngCookies",
            "ui.grid",
            "ui.grid.edit",
            "ui.grid.cellNav",
            "ui.grid.selection",
            "ngAutocomplete",
            "ngMap",
            "wu.staticGmap",
            "mgcrea.ngStrap",
            "ngImgCrop",
            "ui.bootstrap",
            "ui.bootstrap.alert",
            "ui.bootstrap.collapse",
            "ui.bootstrap.tabs",
            "ngTable",
            "nvd3ChartDirectives",
            "oitozero.ngSweetAlert",
            "google.places",
            "angularFileUpload",
            "base64",
            "ngDialog",
            "ui.mask",
            'ui.bootstrap.modal', "template/modal/backdrop.html","template/modal/window.html",
            "ui.bootstrap.tabs", "template/tabs/tabset.html", "template/tabs/tab.html"
        ]
    );


    app.config(function($routeProvider, $locationProvider){
        $routeProvider
            .when("/dashboard", {
                templateUrl: "dashboard/dashboard.html",
                css: "dashboard/dashboard.css",
                controller: "DashboardController"
            })
            .otherwise({
                redirectTo:"/login"
            });

    });
    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    });


    app.controller('LayoutCtrl', function ($scope) {

        $scope.toggleSidebar = function () {
            var $ = angular.element;
            if ($(window).width() <= 992) {
                $('.row-offcanvas').toggleClass('active');
                $('.left-side').removeClass('collapse-left');
                $('.right-side').removeClass('strech');
                $('.row-offcanvas').toggleClass('relative');
            } else {
                /* Else, enable content streching*/
                $('.left-side').toggleClass('collapse-left');
                $('.right-side').toggleClass('strech');
            }
        };

    });


app.factory("auth", function($rootScope,$cookies,$cookieStore,$location, $http, $log)
{
    return{
        login : function(username, token, chat_token, params, app_id)
        {
            //creamos la cookie con el nombre que nos han pasado

            $cookies.username = username;
            $cookies.token = token;
            $cookies.chat_token = chat_token;
            $cookies.app_id = app_id;
            QB.init(chat_token);
            //$log.log(params);
            //Inicio de sesión quickblox
            $location.path("/home");
            $rootScope.showMenu = true;
            QB.login(params, function(err, result) {
              // callback function
              if(err){
               // $rootScope.$broadcast("LOGOUT", "cerrar");
              }else{
                $cookies.QBUser_id = result.id;
                var jid = QB.chat.helpers.getUserJid($cookies.QBUser_id, app_id);
                $cookies.jid = jid;
                $log.log(result);
                QB.chat.connect({jid: jid, password: chat_token}, function(err, roster) {
                  if(err){

                  }else{
                    $cookies.estado = "online";

                      $log.log("SESION " + QB.getSession());

                      var onDialogs = function(err, res) {
                      // callback function
                      var contactsDialogs = res.items.length;

                      $rootScope.$broadcast("DIALOGS", contactsDialogs);
                      $rootScope.$broadcast("DIALOGSsss", contactsDialogs);

                    };
                    QB.chat.dialog.list({limit: 90}, onDialogs);

                  }

                });
              }

            });


            //mandamos a la home


        },
        logout : function()
        {
            //al hacer logout eliminamos la cookie con $cookieStore.remove
            $log.log("CERRANDO...");
            $rootScope.showMenu=false;
            $cookieStore.remove("username");
            $cookieStore.remove("token");
            $cookieStore.remove("chat_token");
            $cookieStore.remove("estado");
            $cookieStore.remove("app_id");
            $cookieStore.remove("QBUser_id");
             // $cookieStore.remove("jid");
            QB.chat.disconnect();
            //mandamos al login
            $location.path("/login");
            //$rootScope.showMenu=false;
        },
        QBRefreshChat: function()
        {
          if(typeof($cookies.jid) != "undefined" && typeof($cookies.chat_token) != "undefined"){
            QB.init($cookies.chat_token);
            QB.chat.connect({jid: $cookies.jid, password: $cookies.chat_token}, function(err, roster) {
                  if(err){

                  }else{
                    $cookies.estado = "online";



                    var onDialogs = function(err, res) {
                      // callback function
                      var contactsDialogs = res.items.length;

                      $rootScope.$broadcast("DIALOGS", contactsDialogs);


                    };
                    QB.chat.dialog.list({limit: 90}, onDialogs);

                  }

                });

          }

        },
        updateContactsDialog : function(numContacts)
        {//$rootScope.dialogos = 9;
          $rootScope.$apply(function () {
            $rootScope.dialogos = numContacts;
          });
        },

        checkStatus : function()
        {
            //creamos un array con las rutas que queremos controlar
            var rutasPrivadas = ["/dashboard","/promociones","/chat", "/llamadas", "/contacts"];
            if(this.in_array($location.path(),rutasPrivadas) && typeof($cookies.username) == "undefined")
            {
                $location.path("/login");
            }
            //en el caso de que intente acceder al login y ya haya iniciado sesión lo mandamos a la home
            else if($location.path() == "/login" && typeof($cookies.username) != "undefined")
            {
                $location.path("/dashboard");
                $rootScope.showMenu=true;
                $rootScope.username=$cookies.username;
                $log.log($rootScope.dialogos);
                this.QBRefreshChat();

            }
            //en el caso de que intente acceder a la ruta privada y ya haya iniciado sesión lo mandamos a la home
            else if(this.in_array($location.path(),rutasPrivadas) && typeof($cookies.username) != "undefined")
            {
                $rootScope.username=$cookies.username;
                $rootScope.showMenu=true;
                //$log.log($rootScope.dialogos);
                
                switch($location.path()) {
                  case "/dashboard":
                     $rootScope.activated = 1;
                     $rootScope.titulo = "Panel Sucursal";
                     $rootScope.icono = "fa fa-dashboard";
                     break;
                  case "/chat":
                      $rootScope.activated = 2;
                      $rootScope.titulo = "Chat";
                      $rootScope.icono = "fa fa-comments-o";
                      break;
                  case "/promociones":
                      $rootScope.activated = 3;
                      $rootScope.titulo = "Promociones";
                      $rootScope.icono = "fa fa-tags";
                      break;
                  case "/llamadas":
                      $rootScope.activated = 4;
                      $rootScope.titulo = "Llamadas";
                      $rootScope.icono = "fa fa-phone";
                      break;
               }
            }
        },
        in_array : function(needle, haystack)
        {
            var key = '';
            for(key in haystack)
            {
                if(haystack[key] == needle)
                {
                    return true;
                }
            }
            return false;
        }
    };
});



    app.run(function ($rootScope, authService) {

        $rootScope.$on('$routeChangeStart', function () {
            authService.checkStatus();
        });

        $rootScope.logout = function () {
            authService.logOut();
        };
        $rootScope.chatListener = null;
    });

    app.provider('AppConfig', function () {
        var config = {
            server: 'http://ec2-54-69-1-179.us-west-2.compute.amazonaws.com/api/',
            clientId: 'tattlerWebApp'
        };
        return {
            set: function (settings) {
                config = settings;
            },
            $get: function () {
                return config;
            }
        };
    });

    app.config(['ngDialogProvider', function (ngDialogProvider) {
        ngDialogProvider.setDefaults({
            className: 'ngdialog-theme-default',
            plain: false,
            showClose: true,
            closeByDocument: false,
            closeByEscape: false
        });
    }]);
    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    });

    app.run(['authService', function (authService) {
        authService.fillAuthData();
    }]);



})();
