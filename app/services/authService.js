﻿'use strict';
var app = angular.module("tattlerApp");
app.factory('authService', ['$http', '$q', 'localStorageService', 'AppConfig', '$rootScope', '$location', '$cookieStore', '$log', 'chatTattler', 'profileBranch', function ($http, $q, localStorageService, AppConfig, $rootScope, $location, $cookieStore, $log, chatTattler, profileBranch) {

    var serviceBase = AppConfig.server;
    var clientId = AppConfig.clientId;
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: "",
        useRefreshTokens: false
    };

    var _externalAuthData = {
        provider: "",
        userName: "",
        externalAccessToken: ""
    };

    var regexPathArray = ["^\/dashboard$",
        "^\/promociones$",
        "^\/chat$",
        "^\/contacts$",
        "^\/llamadas$"
    ];

    var publicRoutes = ["/login"];

    var inValidPath = function (needle, haystack) {
        var key = '';
        for (key in haystack) {
             var resultArray = needle.match(haystack[key]);
             if (resultArray != null && resultArray.length == 1)
             return true

        }
        return false;
    };

    angular.isUndefinedOrNull = function(val) {
        return angular.isUndefined(val) || val === null;
    };

    var in_array = function (needle, haystack) {

        var key = '';
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }

        return false;
    };     

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'account/register', registration).then(function (response) {
            return response;
        });

    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        if (loginData.useRefreshTokens) {
            data = data + "&client_id=" + clientId;
        }

        var deferred = $q.defer();

        $http.post(serviceBase + 'accounts/login', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
            var chat_token = JSON.parse(response.chat_session);


            if (loginData.useRefreshTokens) {
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: response.refresh_token, QBToken: chat_token.token,useRefreshTokens: true });
            }
            else {
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false });
            }
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
            _authentication.useRefreshTokens = loginData.useRefreshTokens;

            $rootScope.showMenu = true;

            $location.path('/dashboard');
            var getProfile = profileBranch.getProfileBranch();
            getProfile.then(function(profile){
                $rootScope.messaging_id = profile.messaging_id;
                $rootScope.profileImage = profile.manager.profile_image;
                $rootScope.username = loginData.userName;
                $rootScope.name = profile.name;
                deferred.resolve(response);
            }, function(err){
                _logOut();

                deferred.reject(err);
            });


        }).error(function (err, status) {
            _logOut();

            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _loginQB = function(loginData, QBtoken, app_id){
        QB.init(QBtoken);
        var deferred = $q.defer();
        var params = {email: loginData.userName, password: loginData.password};
        QB.login(params, function(err, result) {
            // callback function
            if(err){
                deferred.reject(err);
            }else{
                $rootScope.QBUser_id = result.id;
                $rootScope.jid = QB.chat.helpers.getUserJid($rootScope.QBUser_id, app_id);
                $rootScope.app_id = app_id;
                //$rootScope.jid = jid;
                $log.log(result);
               // deferred.resolve(result);
                QB.chat.connect({jid: $rootScope.jid, password: QBtoken}, function(err, result) {
                    if(err){
                        deferred.reject(err);
                    }else{
                        $rootScope.contactsList = [];
                        QB.chat.onMessageListener = function(userId, message) {
                            // callback function
                            if(typeof(userId) != 'undefined' && typeof(message) != 'undefined') {
                                if(_.indexOf($rootScope.contactsList, userId) === -1){
                                    $rootScope.contactsList.push(userId);
                                }
                                $log.log("Usuario: " + userId + " " +message);
                                $log.log($rootScope.contactsList);
                            }

                        };

                        $rootScope.chatListener = QB.chat.addListener({name: 'message', type: 'chat', live: true}, QB.chat.onMessageListener);
                        deferred.resolve(result);

                    }

                });
            }

        });
        return deferred.promise;
    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.useRefreshTokens = false;
        QB.chat.disconnect();
        $rootScope.showMenu = false;                    
        $location.path('/login');

    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _authentication.useRefreshTokens = authData.useRefreshTokens;
        }

    };

    var _refreshToken = function () {
        var deferred = $q.defer();

        var authData = localStorageService.get('authorizationData');

        if (authData) {

            if (authData.useRefreshTokens) {

                var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + clientId;

                localStorageService.remove('authorizationData');

                $http.post(serviceBase + 'accounts/login', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                    localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });

                    deferred.resolve(response);

                }).error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });
            }
        }

        return deferred.promise;
    };

    var _obtainAccessToken = function (externalData) {

        var deferred = $q.defer();

        $http.get(serviceBase + 'api/account/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.useRefreshTokens = false;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _registerExternal = function (registerExternalData) {

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/account/registerexternal', registerExternalData).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.useRefreshTokens = false;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _checkStatus = function () {
        var authData = localStorageService.get('authorizationData');
        var location = $location.path();
        
        if (inValidPath(location, regexPathArray) && (angular.isUndefinedOrNull(authData))) {
            $log.log("En 1");
            $rootScope.showMenu = false;
            $location.path("/login");
        }
        /*en el caso de que intente acceder al login y ya haya iniciado sesión lo mandamos a la home*/
        else if (location == "/login" && (!angular.isUndefinedOrNull(authData))) {
            $log.log("En 2");
            $rootScope.showMenu = true;
            $rootScope.username = authData.userName;                   
            $location.path("/dashboard");

        }
        /*en el caso de que intente acceder a la ruta privada y ya haya iniciado sesión lo mandamos a la home*/
        else if (inValidPath(location, regexPathArray) && (!angular.isUndefinedOrNull(authData))) {
            $rootScope.username = authData.userName;
            $rootScope.showMenu = true;
            $log.log("En 3");


                switch (location) {
                    case "/dashboard":
                        $rootScope.activated = 1;
                        $rootScope.titulo = "Panel Sucursal";
                        $rootScope.icono = "fa fa-dashboard";
                        break;
                    case "/chat":
                        $rootScope.activated = 2;
                        $rootScope.titulo = "Chat";
                        $rootScope.icono = "fa fa-comments-o";
                        break;
                    case "/promociones":
                        $rootScope.activated = 3;
                        $rootScope.titulo = "Promociones";
                        $rootScope.icono = "fa fa-tags";
                        break;
                    case "/llamadas":
                        $rootScope.activated = 4;
                        $rootScope.titulo = "Llamadas";
                        $rootScope.icono = "fa fa-phone";
                        break;
                }



        } else if (in_array(location, publicRoutes)) {
            $rootScope.showMenu = false;
        }
    };

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.refreshToken = _refreshToken;
    authServiceFactory.loginQB = _loginQB;
    authServiceFactory.obtainAccessToken = _obtainAccessToken;
    authServiceFactory.externalAuthData = _externalAuthData;
    authServiceFactory.registerExternal = _registerExternal;

    authServiceFactory.checkStatus = _checkStatus;

    return authServiceFactory;
}]);