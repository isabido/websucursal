(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('authInterceptorService', ['$q', '$injector','$location', 'localStorageService', '$rootScope', '$window', function ($q, $injector,$location, localStorageService, $rootScope, $window) {
        var locationpublic = $location.path();
        var authInterceptorServiceFactory = {};

        var in_array = function (needle, haystack) {

            var key = '';
            for (key in haystack) {
                if (haystack[key] == needle) {
                    return true;
                }
            }

            return false;
        };      

        angular.isUndefinedOrNull = function(val) {
            return angular.isUndefined(val) || val === null 
        };

        var publicRoutes = ["/register", "/login/recover",
                "/login/recoveraccount/:token",
                "/login/activate/:token"];

        var _request = function (config) {

            if (!in_array(locationpublic, publicRoutes)) {
                config.headers = config.headers || {};
       
               var authData = localStorageService.get('authorizationData');
               if (!angular.isUndefinedOrNull(authData)) {
                  config.headers.Authorization = 'Bearer ' + authData.token;
               }
            }

            return config;
        };

        var _responseError = function (rejection) {
            if (rejection.status === 401) {
                var authService = $injector.get('authService');
                var authData = localStorageService.get('authorizationData');

                if (!angular.isUndefinedOrNull(authData) && authData.useRefreshTokens) {
                    authService.refreshToken();
                }                
                else{
                    authService.logOut();
                }
            }
            return $q.reject(rejection);
        };

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;

        return authInterceptorServiceFactory;
    }]);

})();