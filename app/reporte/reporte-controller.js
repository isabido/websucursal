(function () {
  'use strict';
  var app = angular.module("tattlerApp");
    app.controller('myModalControllerReporte', ['$scope','$modalInstance','Item', '$cookies', '$log', '$http', 'userReport', function($scope, $modalInstance,Item, $cookies, $log, $http, userReport)
    {
        var customerId = Item;
        $log.log(Item);
        $scope.reason = "";
        $scope.reasons = {
            fomenta: 'El usuario fomenta el contenido comercial no deseado.',
            conducta: 'El usuario tiene conducta dañina hacia la empresa.',
            contenido: 'Otro/Contenido abusivo.'
        };

        $scope.alerts = [];

        $scope.addAlert = function() {

        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.save = function(){
            $scope.alerts = [];
            $log.log($scope.reason);
            var post = userReport.setReport(customerId, $scope.reason);
            post.then(function(response){
                $log.log(response);
            }, function(err){
                $scope.alerts.push({type: 'danger', msg: err.Message});

            });
        };

        $scope.cancel = function ()
        {
            $log.log("Cancelado");
            $modalInstance.dismiss('cancel');
        };

    }]);
})();