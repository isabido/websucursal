(function () {
  'use strict';
  var app = angular.module("tattlerApp");
  app.controller("ChatController", function($scope, $http, $rootScope,$cookies, $log, chatTattler, $timeout, authService, localStorageService, $modal){

      var route = $scope.$root.server + "branch/contacts";
      $scope.chatCustomersContacts = [];
      $scope.chatCompanyContacts = [];
      $scope.chatMes = "";
      //$scope.receiverId = $rootScope.messaging_id;
      $scope.activeContac = {
          messaging_id: 0,
          customer_id: 0,
          name: '',
          jid: '',
          image: '',
          conversationId: 0
      };


      $scope.infoActiveContact = {};
      $scope.isCollapsed = true;
      $scope.attachShow = true;
      $scope.infoActiveContact = {};
      var clearData = function(){
          $scope.infoActiveContact = {
              customer_id: "",
              full_name: "",
              addresses: [
                  {
                      street_line1: "",
                      street_line2: "",
                      street_line3: "",
                      external_number: "",
                      internal_number: "",
                      neighborhood: "",
                      zip_code: "",
                      references: ""
                  },
                  {
                      street_line1: "",
                      street_line2: "",
                      street_line3: "",
                      external_number: "",
                      internal_number: "",
                      neighborhood: "",
                      zip_code: "",
                      references: ""
                  }
              ],
              phone: "",
              mobile_phone: "",
              email: "",
              location: {
                  longitude: 0,
                  latitude: 0
              }
          };
      };



      $scope.chatBot = function(){
          $log.log("Activado");
          $scope.singleModel = 1;
          $scope.menuTittleChatBot = "Activar Bot";
      };

      angular.isUndefinedOrNull = function(val) {
          return angular.isUndefined(val) || val === null;
      };

      $scope.openModalBot = function ()
      {
          var bot =  localStorageService.get('chatResponse');
          if(angular.isUndefinedOrNull(bot)){
              $log.log("En modale chat");
              var modalInstance = $modal.open({
                  templateUrl: 'chat/html/automaticMessage.html',
                  controller: 'myModalControllerAutomaticChat',
                  resolve: {
                      Item: function() //scope del modal
                      {
                          return "";
                      }
                  }
              });

              modalInstance.result.then(function (selectedItem) {
                  if(selectedItem.active){
                      $log.log(selectedItem.messageResponse);
                      $scope.automaticMsg = selectedItem.messageResponse;
                      $scope.singleModel = true;
                      $scope.menuTittleChatBot = "Desactivar";
                  }
              }, function () {
                  $log.info('Modal dismissed at: ' + new Date());
              });
          }else if(bot.active){
              localStorageService.remove('chatResponse');
              $scope.singleModel = false;
              $scope.menuTittleChatBot = "Activar Respuesta";

          }else{
              $log.log("En modale chat");
              var modalInstance = $modal.open({
                  templateUrl: 'chat/html/automaticMessage.html',
                  controller: 'myModalControllerAutomaticChat',
                  resolve: {
                      Item: function() //scope del modal
                      {
                          return "";
                      }
                  }
              });
              modalInstance.result.then(function (selectedItem) {
                  if(selectedItem.active){
                      $scope.automaticMsg = selectedItem.messageResponse;
                      $scope.singleModel = true;
                      $scope.menuTittleChatBot = "Desactivar";
                  }
              }, function () {
                  $log.info('Modal dismissed at: ' + new Date());
              });
          }


      };


      $scope.openModal = function (data)
      {
        $log.log(data);
        var modalInstance = $modal.open({
              templateUrl: 'reporte/reporte.html',
              controller: 'myModalControllerReporte',
              resolve: {
                Item: function() //scope del modal
              {
                return data;
              }
            }
        });
      };

      $scope.openModalImage = function (url) {


              $log.log("En modal img");
              var modalInstance = $modal.open({
                  templateUrl: 'chat/html/img/chatImg.html',
                  controller: 'modalImageController',
                  resolve: {
                      Item: function () //scope del modal
                      {
                          return url;
                      }
                  }
              });

      };
        //Inicializa el chat
      $scope.initChat = function(){
          if(typeof(QB.chat) != 'undefined' || QB.chat!= null) {
              $scope.chatListener();
              var par = {
                  full_name: $rootScope.username,
                  email: 'shotokan.isc@gmail.com',
                  lat: '18°69',
                  long: '26°98,56'
              };
              //chatTattler.sendMessage( $rootScope.jid, 'hola mundo', par);
              $scope.getContacts();
              //$log.log($rootScope.username);


                  var bot =  localStorageService.get('chatResponse');
                  if (!angular.isUndefinedOrNull(bot)) {
                      if (bot.active) {
                          $scope.singleModel = true;
                          $scope.menuTittleChatBot = "Desactivar";
                          $scope.automaticMsg = bot.messageResponse;
                          $log.log($scope.automaticMsg);
                      } else {
                          $scope.singleModel = false;
                          $scope.menuTittleChatBot = "Activar Bot";
                      }

                  } else {
                      $scope.singleModel = false;
                      $scope.menuTittleChatBot = "Activar Bot";
                  }


          }else{
              authService.logout();
          }
      };

      /*
       * Se obtienen los contactos y se dividen en dos listas, contactos de la compañias y contactos con
       * platicas activas
       */
      $scope.getContacts = function(){
          var contacts = chatTattler.getActiveChatContacts();
          contacts.then(function(response) {
              $log.log(response);
              $scope.chatCompanyContacts = response[0];
              $log.log("Comapny: ");
              $log.log($scope.chatCompanyContacts);
              $scope.chatCustomersContacts = response[1];

              if($scope.activeContac.messaging_id === 0 && $scope.activeContac.customer_id === 0){
                  if(typeof($scope.chatCustomersContacts[0])!== 'undefined') {
                      $scope.chatContact($scope.chatCustomersContacts[0].id, $scope.chatCustomersContacts[0].receiver.messaging_id, $scope.chatCustomersContacts[0].receiver.name, $scope.chatCustomersContacts[0].receiver.profile_image, $scope.chatCustomersContacts[0].receiver.id);
                  }
              }


          }, function(err) {
              $log.log('Promise Rejected with Message: '+err);
          });
      };

      /* Envía los mensajes de la sucursal */
      $scope.submitMessage = function(){
          $log.log($scope.file);
          var par = {
              full_name: $rootScope.name,
              lat: '',
              long: ''
          };
          if(typeof($scope.file) != 'undefined' && $scope.file != null){
              //$log.log($scope.file.name);
              var ext  = $scope.file.name.split('.').pop();
              $log.log(ext);
              var fileType = chatTattler.fileType(ext.toLowerCase());
                $log.log(fileType);
                  var post = chatTattler.uploadFile($scope.file);
                  post.then(function(data){

                      chatTattler.sendFile($scope.activeContac.jid, data.url, par, $scope.activeContac.messaging_id, fileType);
                      if($scope.chatMes!= ""){
                          chatTattler.sendMessage( $scope.activeContac.jid, $scope.chatMes, par, $scope.activeContac.messaging_id, 'Text');
                          $scope.chatMes = "";

                      }
                      $scope.file = null;
                  }, function(err){
                  });
          }else{

              $log.log($scope.activeContac.jid);
              chatTattler.sendMessage( $scope.activeContac.jid, $scope.chatMes, par, $scope.activeContac.messaging_id, 'Text');
              $scope.chatMes = "";
          }

      };

      $scope.sendAutomaticMessage = function(msg, userid){
          var par = {
              full_name: $rootScope.name,
              lat: '',
              long: ''
          };

          chatTattler.sendMessage( $scope.getSenderJid(userid), msg, par, userid, 'Text');
      };

      $scope.submitLocation = function(){
          var par = {full_name: $rootScope.name,
              lat: $rootScope.location.latitude,
              long: $rootScope.location.longitude
          };

          var msg = $rootScope.location.latitude+", "+$rootScope.location.longitude;
          chatTattler.sendFile($scope.activeContac.jid, msg, par, $scope.activeContac.messaging_id, 'Geolocation');
          //chatTattler.sendMessage( $scope.activeContac.jid, loc, par, $scope.activeContac.messaging_id, 'Geolocation');

      };

      /* Se obtienen los datos del usuario activo en el chat (del cual se desplegaran en pantalla los mensajes) */
      $scope.chatContact = function(conversation_id, qb_id, name, profile_image, customer_id){
          $log.log(customer_id);
          $scope.isCollapsed = true;
          clearData();
          chatTattler.cleanChat();
          $scope.activeContac = {
              messaging_id: 0,
              customer_id: 0,
              name: '',
              jid: '',
              image: '',
              conversationId: 0
          };
          $log.log("Customer antes: " + customer_id);
          $scope.file = null;
          $log.log($scope.activeContac);
          $scope.activeContac.jid = $scope.getSenderJid(qb_id);
          $scope.activeContac.conversationId = conversation_id;
          $scope.activeContac.messaging_id = qb_id;
          $scope.activeContac.customer_id = customer_id;
          var get = chatTattler.getConversation($scope.activeContac.conversationId, $scope.activeContac.messaging_id);

          get.then(function(data){
              $log.log(data);

              $scope.activeContac.image = profile_image;

              $scope.activeContac.name = name;
              $log.log("customer"+$scope.activeContac.customer_id);
              chatTattler.listMessages(data, name, $scope.activeContac.image);

              var get_contact = chatTattler.getContactInfo($scope.activeContac.customer_id);
              get_contact.then(function(contact){
                $log.log(contact);
                  chatTattler.markMessagesAsRead($scope.activeContac.conversationId);
                  $scope.infoActiveContact = contact;
                  chatTattler.markMessagesAsRead($scope.activeContac.conversationId);
                  $scope.isCustomer = true;
                  $scope.infoActiveContact = contact;

              }, function(err){

                  chatTattler.markMessagesAsRead($scope.activeContac.conversationId);
                  $scope.isCustomer = false;
                  $scope.infoActiveContact.customer_id = $scope.activeContac.customer_id;
                $log.log(err);
              });

          }, function(err){
              $log.log(err);
          });
      };

      /* Se obtiene el JID del usuario */
      $scope.getSenderJid = function(senderId){
          return QB.chat.helpers.getUserJid(senderId, $rootScope.app_id);
      };

      //Se registran o editan los datos del contacto actual en el chat
      $scope.registerContact = function(){

          var postContact = chatTattler.registerContact($scope.infoActiveContact);
          postContact.then(function(response){
              $log.log("Se han registrado los datos");
              $scope.isCollapsed = true;
              $scope.isCustomer = true;

          }, function(err){
              $log.log("no se han guardado los datos");

          });
      };
        //se actualizan los datos del usuario contacto
      $scope.updateContact = function(){
            $log.log("en update controller");
          $log.log($scope.infoActiveContact);
          var putContact = chatTattler.updateContact($scope.infoActiveContact);
          putContact.then(function(response){
              $log.log("Se han registrado los datos");
              $scope.isCollapsed = true;
              $scope.isCustomer = true;

          }, function(err){
              $log.log("no se han guardado los datos");

          });
      };
      /* Se agrega un Listener para recepción de los mensajes provenientes de quickblox*/
      $scope.chatListener = function(){
          QB.chat.onMessageListener = function(userId, message) {
              if(typeof(userId) != 'undefined' && typeof(message) != 'undefined') {
                  if (userId === $scope.activeContac.messaging_id) {
                      chatTattler.receiveMessage(userId, message, $scope.activeContac.messaging_id,  $scope.activeContac.name, $scope.activeContac.image);
                  } else {
                      //$log.log("Buscar usuario");
                      //$log.log(chatTattler.searchUserInList($scope.chatCompanyContacts, userId));
                      if(_.indexOf($rootScope.contactsList, userId) === -1){
                          $rootScope.contactsList.push(userId);
                      }
                      if(chatTattler.searchUserInList($scope.chatCompanyContacts, userId)){
                          chatTattler.markContact(userId,"company");

                      }else if(chatTattler.searchUserInList($scope.chatCustomersContacts, userId)){
                          chatTattler.markContact(userId, "client");
                      }else{

                          $scope.getContacts();
                      }

                  }
                  $log.log($scope.singleModel);
                  if($scope.singleModel){
                      $log.log("Respuesta " + $scope.automaticMsg);
                      if(typeof($scope.automaticMsg)!== 'undefined' && $scope.automaticMsg !== ''){
                          $log.log($scope.automaticMsg);
                          $scope.sendAutomaticMessage($scope.automaticMsg, userId);
                      }

                  }
              }

          };

          if(typeof($rootScope.chatListener) != 'undefined' && $rootScope.chatListener != null)
          {
              QB.chat.deleteListener($rootScope.chatListener);
              $log.log("deleteListener");
          }
          $rootScope.chatListener = QB.chat.addListener({name: 'message', type: 'chat', live: true}, QB.chat.onMessageListener);


      };

      var searchContact_qb_id = function(messaging_id){
          if($scope.activeContac.messaging_id !== messaging_id){

          }
      };



      $scope.fileType = function(file){
          switch (file) {
              case 'docx':
              case 'doc':
                  return 'docx6.png';
              case 'odt':
                  return 'odt5.png';
              case 'xml':
                  return 'xml5.png';
              case 'mp3':
              case 'wav':
              case 'ogg':
              case 'wma':
                  return 'musical7.png';
              case 'avi':
              case 'mpeg':
              case 'mov':
              case 'wmv':
              case 'mp4':
              case '3gp':
                  return 'film51.png';
              case 'jpg':
              case 'png':
              case 'jpeg':
              case 'gif':
              case 'svg':
                  return 'image79.png';
              case 'pdf':
                  return 'pdf19.png';
              case 'xls':
              case 'xlsx':
                  return 'xlsx5.png';
              case 'ppt':
              case 'pptx':
                  return 'pptx.png';
              case 'zip':
                  return 'zip17.png';
              case 'rar':
                  return 'rar4.png';
              default:
                  return 'text70.png';

          }

      };

      $scope.closeAttach = function(){
          $scope.attachShow = !$scope.attachShow;
          $log.log("EN EL CLOSE");


      };

      var onSuccessGeolocating = function(position){
        $log.log(position);
         // chatTattler.receiveVideo(position.coords.latitude, position.coords.longitude);

      };

      var onErrorGeolocating = function(error){
          $log.log(error);
      };

      $scope.getLocalization= function(){
          if(navigator.geolocation)
          {
              var options =  {
                  enableHighAccuracy: false,
                  maximumAge:         5000,
                  timeout:            10000
              };
              navigator.geolocation.getCurrentPosition(onSuccessGeolocating, onErrorGeolocating, options);
          }
          else
          {
              // No se cuenta con soporte para geolocalización, manejar la situación.
          }
      };

      updateTime();
      $scope.initChat();
     // $scope.getLocalization();

  });


    app.config(function ($routeProvider) {
        $routeProvider
            .when("/chat", {
                templateUrl: "chat/index.html",
                css: "chat/assets/css/style.css",
                controller: "ChatController"
            });
    });

    app.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });

    app.directive('uploaderModel', ['$parse', function($parse){
        return{
            restrict: 'A',
            link: function(scope, element, attrs){
                /* Se crea un evento que esta esperando el cambio del elemento input:file */
                element.on("change", function(e){
                    /* asigna el archivo al modelo file que se encuentra en el atributo */
                    $parse(attrs.uploaderModel).assign(scope, element[0].files[0]);
                    console.log(scope.file);
                    var ext  = scope.file.name.split('.').pop();
                    console.log(ext);
                    scope.attachShow = true;
                    var showFile ='<span class="file"><img src="components/app-data/img/icons/files/'+scope.fileType(ext.toLowerCase());
                    showFile += '" heigth="20" width="20" /> ' + scope.file.name + '<button type="button" class="close">';
                    showFile += '&times;</button></span>';
                    $('.attach').show().html(showFile);
                    /* se crea un evento para poder cancelar el archivo */
                    $('.attach').on('click', '.close', function(){
                        $('.attach').hide();
                        $('input:file').val('');
                        scope.file = null;
                        //console.log("Nombre " + scope.file.name);
                    });


                });
            }
        };
    }]);



})();
