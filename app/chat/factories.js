(function () {

  'use strict';

  var app = angular.module("tattlerApp");

  app.factory('chatTattler', ['$resource', '$cookies', 'AppConfig', '$rootScope', '$log', '$http', '$q',function ($resource, $cookies, AppConfig, $rootScope, $log, $http, $q) {
      var chatServiceFactory = {};
      var receiverUserId = 0;
      var serviceBase = AppConfig.server;

      /* Se prepara el mensaje, se envía a quickblox, se despliega en pantalla y se guarda en tattler */
      var sendMessage = function(jid, message, params, messaging_id){
          var date = new Date().toISOString();


          //$log.log("Factoria");
          //$log.log(QB.chat.helpers.getIdFromNode(jid));
          var save = this.saveMessage(message, messaging_id, 'Text');
          save.then(function(response){
              var message2send = {
                  type: 'chat',
                  body: message,
                  // any custom parameters
                  extension: {
                      full_name: params.full_name,
                      email: params.email,
                      save_to_history: 1,
                      read: 0,
                      date_sent: date,
                      lat: params.lat,
                      long: params.long,
                      message_type: 'Text',
                      tattler_message_id: response.id
                  }
              };
              QB.chat.send(jid, message2send);
              receiveMessage(messaging_id, message2send, 'Text', params.full_name);
              $log.log(response);
          }, function(err){
              $log.log(err);
          });

      };

      var sendFile = function(jid, message, params, messaging_id, fileType){
          var date = new Date().toISOString();

          if(fileType==='Geolocation'){
              var location = 'https://www.google.com.mx/maps/search/'+params.lat+','+params.long+'/@'+params.lat+','+params.long+',17z';

          }
          var save = this.saveMessage(message, messaging_id, fileType);
          save.then(function(response){

              $('.attach').hide();
              $('input:file').val('');
              $log.log(response);
              var message2send = {
                  type: 'chat',
                  body: message,
                  // any custom parameters
                  extension: {
                      full_name: params.full_name,
                      email: params.email,
                      save_to_history: 1,
                      read: 0,
                      date_sent: date,
                      latitud: params.lat,
                      longitud: params.long,
                      message_type: fileType,
                      tattler_message_id: response.id
                  }
              };
              QB.chat.send(jid, message2send);
              receiveMessage(messaging_id, message2send, jid, params.full_name);
          }, function(err){
              $log.log(err);
          });
      };

      /* Revisar si usuario se encuentra en la lista */
      var searchUserInList = function(contactList, qb_id){
          for(var c in contactList){
              if(_.isMatch(contactList[c].receiver, {messaging_id: qb_id})){
                  return true;
              }

          }
          return false;
      };

      /* Cambiar de color a usuario de la lista que ha enviado un mensaje y no es usuario activo */
      var markContact = function(messaging_id, type){
          var user = $('#qb'+messaging_id);
         // $rootScope.contactsList.push(messaging_id);
          $log.log(user);
          if(user.length>0){
              if(type === "client"){
                  user.addClass('unread_client');
                  $log.log("unread_client");
              }else{
                  $log.log("unread_message");
                  user.addClass('unread_message');
              }

          }
      };

      /* Cuando el mensaje es un video se despliega en el chat con el formato correspondiente */
      var receiveMessage = function(userId, message, user, user_name, image_contact){

          if(typeof(userId) != 'undefined' && typeof(message) != 'undefined'){
              //$log.log("Usuario: " +userId + " receriver: "+receiverId + " " +message.body);
              if(user_name === $rootScope.name){

                var html = '<div class="chat-box-right">';
                var html_image = '<div class="chat-box-name-right">';
                var prof_img = $rootScope.profileImage;
                  //html += '<section class="messageSender">';
                  //html += '<header class="nameSender"><span><b>' + user_name + '</b></span></header>';
                  //html += '<div class="globo">';
              }else if(userId === user){
                var html = '<div class="chat-box-left">';
                var html_image = ' <div class="chat-box-name-left">';
                var prof_img = image_contact;
                  //var html = '<section class="messageReceiver">';
                  //html += '<header class="nameReceiver"><span><b>' + user_name + '</b></span></header>';
                  //html += '<div class="globo2">';
              }
              $log.log("En receive " + message.body);
              $log.log(message.extension.fileType);
              var selector = $('.chat .messages');



              switch (message.extension.message_type){
                  case 'Text':
                      html += '<div class="message-description">' + message.body + '</div>';
                      html += '<time datetime="' + message.extension.time + '">' + $.timeago(message.extension.date_sent ) + '</time>';
                      break;
                  case 'Video':
                      html += '<div class="message-description"><div class="video"> <video controls src="' + message.body + '">Tu navegador no soporta HTML5 </video></div></div>';
                      html += '<time datetime="' + message.extension.time + '">' + $.timeago(message.extension.date_sent ) + '</time>';
                      break;
                  case 'Audio':
                      html += '<div class="message-description"><audio controls src="' + message.body + '"></aundio></div>';
                      html += '<time datetime="' + message.extension.time + '">' + $.timeago(message.extension.date_sent ) + '</time>';
                      break;
                  case 'Image':
                      html += '<div class="message-description"><a class="chat-modal" href="'+message.body+'" download target="_blank"><img class="imgMessage" width="200" heigth="200" src="' + message.body + '" /></a></div>';
                      html += '<time datetime="' + message.extension.time + '">' + $.timeago(message.extension.date_sent ) + '</time>';
                      break;
                  case 'File':
                      var ext  = message.body.split('.').pop();
                      var icon = checkFileType(ext.toLowerCase());
                      html += '<div class="message-description"><a href="' + message.body;
                      html += '"><img src="components/app-data/img/icons/files/' + icon + '" class="imgFile" /></a></div>';
                      html += '<time datetime="' + message.extension.time + '">' + $.timeago(message.extension.date_sent ) + '</time>';
                      break;
                  case 'Promotion':
                      var promotion = JSON.parse(message.body);
                      html += '<div class="message-description"><a class="chat-modal" href="'+promotion.image+'" download target="_blank"><img class="imgMessage" width="200" heigth="200" src="' + promotion.image + '" /></a></div>';
                      html += '<p style="text-align: center;">'+promotion.title+'</p></div>';
                      html += '<time datetime="' + message.extension.time + '">' + $.timeago(message.extension.date_sent ) + '</time>';
                      break;
                  case 'Geolocation':
                      var location = message.body.split(",");
                      var loc = "http://maps.googleapis.com/maps/api/staticmap?center="+location[0]+","+location[1]+"&zoom=17&size=537x437&key=AIzaSyB9oHzp0DtDqkfb4PCxFNRf9Yyy9-9Z8Lo";
                      //var Googlemap = 'https://www.google.com.mx/maps/search/'+params.lat+','+params.long+'/@'+params.lat+','+params.long+',17z';

                      html += '<div class="message-description"><a href="https://www.google.com.mx/maps/search/'+message.extension.latitud+','+message.extension.longitud+'/@'+message.extension.latitud+','+message.extension.longitud+',17z" target="_blank"><img class="imgMessage" width="400" heigth="400" src="' + loc + '" /></a></div>';
                      html += '<time datetime="' + message.extension.time + '">' + $.timeago(message.extension.date_sent ) + '</time>';
                      break;
              }
              html += '</div>' + html_image;
              html += '<img src="' + prof_img + '" alt="bootstrap Chat box user image" class="img-circle img-circle-chat" /> - '+user_name+'</div>';
              html += '<hr class="hr-clas" />';
              //html += '</div></section><div class="clear"></div>';

              selector.append(html);
              // selector.find('.message:even').addClass('white');
              selector.scrollTo('*:last', 0);
              $( ".chat-modal").unbind( "click" );
              //actualizar();
          }
          selector.scrollTo('*:last', 0);

      };


      /* Cuando el mensaje es una localización se despliega en el chat con el formato correspondiente */
      var receiveLocation = function(lat, long){
          var loc = "<img src='http://maps.googleapis.com/maps/api/staticmap?center="+lat+","+long+"&zoom=13&size=237x137&key=AIzaSyB9oHzp0DtDqkfb4PCxFNRf9Yyy9-9Z8Lo' />";


          //$log.log("Usuario: " +userId + " receriver: "+receiverId + " " +message.body);
          var html = '<section class="message">';
          html += '<header><b></b>';
          html += '<time datetime=""></time></header>';
          html+= '</header>';
          html += '<div class="message-description">' + loc + '</div>';
          html += '</section>';
          var selector = $('.chat .messages');
          selector.append(html);
          selector.find('.message:even').addClass('white');
          selector.scrollTo('*:last', 0);

      };

      /* Limpia la pantalla del chat */
      var cleanChat = function(){
          $('.chat .messages').empty();

      };

      /* Cuando el mensaje es una localización, se despliega con el formato correspondiente */
      var receiveLocation = function(params){

      };




      /* Se cierra la sesión de quickblox con el chat */
      var disconnectChat = function(){
          QB.chat.disconnect();
      };

      /* Se confirma al usuario para agregar a la lista de contactos */
      var confirmRequestListener = function(){

      };

      /* Se obtienen los usuarios de conversaciones activas con la sucursal */
      var getActiveChatContacts = function(){
          var deferred=$q.defer();
          var customerContacts = [];
          var contacts = [];
          var companyContacts = [];
          $http.get(serviceBase + "conversations")
              .success(function(response, status, headers, config) {
                  var r;
                  for(r in response){
                      $log.log(response[r].receiver.is_customer);
                      if(response[r].receiver.is_customer === false && response[r].receiver.name !== "Administrator"){

                          companyContacts.push(response[r]);
                      }else if(response[r].receiver.is_customer === true){
                          $log.log('else');
                          customerContacts.push(response[r]);
                      }
                  }
                  $rootScope.activeConversations = response.length;
                  contacts.push(companyContacts);
                  contacts.push(customerContacts);
                  deferred.resolve(contacts);
              })
              .error(function(err, status, headers, config) {
                  deferred.reject(err);
              });
          return deferred.promise;
      };

      /* Se guarda el mensaje en tattler */
      var saveMessage = function(message, messaging_id, type){
          $log.log(messaging_id);
          var deferred=$q.defer();
          var route = serviceBase + "conversations/messages";
          var data={
              body: message,
              messaging_id: messaging_id,
              type: type
          };
          $http.post(route,  data, {ignoreLoadingBar: true})
          .success(function(response, status, headers, config){
              deferred.resolve(response);
          }).error(function(err, status, headers, config){
              deferred.reject(err);
          });
          return deferred.promise;
      };

      /* Se obtiene la conversación del usuario activo con la sucursal, para desplegar en pantalla */
      var getConversation = function(conversation_id, messaging_id){
          var deferred=$q.defer();
          var route = serviceBase + "conversations/"+conversation_id+"/messages";

          if($rootScope.contactsList.length > 0){
              $rootScope.contactsList = _.without($rootScope.contactsList, messaging_id);
          }

          $http.get(route).success(function(response, status, headers, config){
              $log.log("remover");
              $log.log($('#qb'+messaging_id).hasClass("unread_client"));
              if($('#qb'+messaging_id).hasClass("unread_message")){
                  $('#qb'+messaging_id).removeClass("unread_message");
              }else{
                  $('#qb'+messaging_id).removeClass("unread_client");
              }

              deferred.resolve(response);
          }).error(function(err, status, headers, config){
              deferred.reject(err);
          });
          return deferred.promise;
      };

      /* Lista los mensajes en pantalla */
      var listMessages = function(messages, activeContact, prof_img){
          var message=null;
          var selector = $('.chat .messages');

          for (message in messages){
              $log.log($rootScope.name);
              if(messages[message].sender === $rootScope.name){
                  var html = '<div class="chat-box-right">';
                  var html_image = '<div class="chat-box-name-right">';
                  var user_image =$rootScope.profileImage;
                  //var html = '<section class="messageSender">';
                  //html += '<header class="nameSender"><span><b>' + messages[message].sender + '</b></span></header>';
                  //html += '<div class="globo">';
              }else if(messages[message].sender === activeContact){
                var html = '<div class="chat-box-left">';
                var html_image = ' <div class="chat-box-name-left">';
                var user_image =prof_img;
                  //var html = '<section class="messageReceiver">';
                  //html += '<header class="nameReceiver"><span><b>' + messages[message].sender + '</b></span></header>';
                  //html += '<div class="globo2">';
              }

              switch (messages[message].type){
                  case 'Text':
                      html += '<div class="message-description">' + messages[message].body + '</div>';
                      html += '<time datetime="' + messages[message].sent_at+ '">' + $.timeago(messages[message].sent_at) + '</time>';
                      break;
                  case 'Video':
                      html += '<div class="message-description"><div class="video"> <video controls src="' + messages[message].body + '">Tu navegador no soporta HTML5 </video></div></div>';
                      html += '<time datetime="' + messages[message].sent_at+ '">' + $.timeago(messages[message].sent_at) + '</time>';
                      break;
                  case 'Audio':
                      html += '<div class="message-description"><audio controls src="' + messages[message].body + '"></aundio></div>';
                      html += '<time datetime="' + messages[message].sent_at+ '">' + $.timeago(messages[message].sent_at) + '</time>';
                      break;
                  case 'Image':
                      html += '<div class="message-description"><a class="chat-modal" href="'+messages[message].body+'" target="_blank"><img class="imgMessage" width="200" heigth="200" src="' + messages[message].body + '" /></a></div>';
                      html += '<time datetime="' + messages[message].sent_at+ '">' + $.timeago(messages[message].sent_at) + '</time>';
                      break;
                  case 'Promotion':
                      var promotion = JSON.parse(messages[message].body);
                      $log.log(promotion);
                      html += '<div class="message-description"><a class="chat-modal" href="'+promotion.image+'" download target="_blank"><img class="imgMessage" width="200" heigth="200" src="' + promotion.image + '" /></a>';
                      html += '<p style="text-align: center;">'+promotion.title+'</p></div>';
                      html += '<time datetime="' + messages[message].sent_at+ '">' + $.timeago(messages[message].sent_at) + '</time>';
                      break;
                  case 'File':
                      var ext  = messages[message].body.split('.').pop();
                      var icon = checkFileType(ext.toLowerCase());
                      html += '<div class="message-description"><a href="' + messages[message].body;
                      html += '"><img src="components/app-data/img/icons/files/' + icon + '" class="imgFile" /></a></div>';
                      html += '<time datetime="' + messages[message].sent_at+ '">' + $.timeago(messages[message].sent_at) + '</time>';
                      break;
                  case 'Geolocation':
                      var location = messages[message].body.split(",");
                      var loc = "http://maps.googleapis.com/maps/api/staticmap?center="+location[0]+","+location[1]+"&zoom=17&size=537x437&key=AIzaSyB9oHzp0DtDqkfb4PCxFNRf9Yyy9-9Z8Lo";
                      var Googlemap = 'https://www.google.com.mx/maps/search/'+location[0]+','+location[1]+'/@'+location[0]+','+location[1]+',17z';

                      html += '<div class="message-description"><a href="'+Googlemap+'" target="_blank"><img class="imgMessage" width="400" heigth="400" src="' + loc+ '" /></a></div>';
                     html += '<time datetime="' + messages[message].sent_at + '">' + $.timeago(messages[message].sent_at) + '</time>';
                      break;
              }
              html += '</div>' + html_image;
              html += '<img src="'+ user_image +'" alt="bootstrap Chat box user image" class="img-circle img-circle-chat" /> - '+messages[message].sender+'</div>';
              html += '<hr class="hr-clas" />';

              selector.append(html);
              selector.scrollTo('*:last', 0);
              $( ".chat-modal").unbind( "click" );
              //actualizar();
          }
          selector.scrollTo('*:last', 0);


      };

      var checkFileType = function(ext){
          switch(ext){
              case 'doc':
              case 'docx':
                  return 'microsoft5.png';
              case 'pdf':
                  return 'pdf19.png';
              case 'odt':
                  return 'odt5.png';
              case 'ppt':
              case 'pptx':
                  return 'pptx1.png';
              case 'xls':
              case 'xlsx':
                  return 'microsoftexcel.png';
              case 'zip':
                  return 'zip6.png';
              case 'rar':
                  return 'rar6.png';
              case 'xml':
                  return 'xml5.png';
              case 'txt':
                  return 'txt2.png';
              case 'pub':
                  return 'pub.png';
              case 'exe':
                  return 'files9.png';
              default:
                  return 'unknown2.png';
          }

      };

      var fileType = function(ext){
          $log.log("FILE TYPE: " +ext);
          if(/^(avi|mpeg|mov|3gp|mp4|wmv)$/.test(ext)){
              return 'Video';
          }else if(/^(jpg|gif|png|jpeg|svg)$/.test(ext)){
              return 'Image';
          }else if(/^(mp3|wav|ogg|wma)$/.test(ext)){
              return 'Audio';
          }else if (/^(mp3|wav|ogg|wma)$/.test(ext)){
              return 'Audio';
          }else if(/^(docx|doc|odt|xml|pdf|txt|json|xls|xlsx|ppt|pptx|pub|zip|rar|7zip|exe)$/.test(ext)){
              return 'File';
          }else{
              return 'File';
          }

      };

      var getContactInfo = function(customer_id){
          var deferred=$q.defer();
          var route = serviceBase + "branch/contacts/" + customer_id;

          $http.get(route, {ignoreLoadingBar: true}).success(function(response, status, headers, config){
              $('#add-contact').css("color", 'green');
              deferred.resolve(response);
          }).error(function(err, status, headers, config){
              $('#add-contact').css("color", 'blue');
              deferred.reject(err);
          });
          return deferred.promise;
      };

      var markMessagesAsRead = function(conver_id){
          var deferred=$q.defer();
          var route = serviceBase + "conversations/"+conver_id+"/messages/reading";
          var data = {conversation_id: conver_id};
          $http.put(route, data,{ignoreLoadingBar: true}).success(function(response, status, headers, config){

              deferred.resolve(response);
          }).error(function(err, status, headers, config){


              deferred.reject(err);
          });
          return deferred.promise;

      };

      var uploadFile = function(file){
          var deferred=$q.defer();
          var route = serviceBase + "conversations/files";
          var formData = new FormData();
          formData.append("file", file);
          $('.chat .input-group').hide();
          $('.file-loading').show();

          $http.post(route, formData,{headers: {"Content-type": undefined}, transformRequest: formData}).success(function(response, status, headers, config){
              $('.attach').hide();
              $('input:file').val('');
              $('.file-loading').hide();
              $('.chat .input-group').show();
              deferred.resolve(response);
          }).error(function(err, status, headers, config){
              $('.attach').hide();
              $('input:file').val('');
              $('.file-loading').hide();
              $('.chat .input-group').show();
              deferred.reject(err);
          });
          return deferred.promise;

      };

      var registerContact = function(contact){
          var deferred=$q.defer();
          var route = serviceBase + "branch/contacts";

          $http.post(route, contact).success(function(response, status, headers, config){

              deferred.resolve(response);
          }).error(function(err, status, headers, config){

              deferred.reject(err);
          });
          return deferred.promise;
      };

      var updateContact = function(contact){
          var deferred=$q.defer();
          $log.log("Update " + contact.id);
          var route = serviceBase + "branch/contacts/" + contact.id;

          $http.put(route, contact).success(function(response, status, headers, config){

              deferred.resolve(response);
          }).error(function(err, status, headers, config){

              deferred.reject(err);
          });
          return deferred.promise;
      };

      chatServiceFactory.sendMessage = sendMessage;
      //chatServiceFactory.receiveVideo = receiveVideo;
      chatServiceFactory.disconnectChat = disconnectChat;
      chatServiceFactory.receiverUserId = receiverUserId;
      chatServiceFactory.receiveMessage = receiveMessage;
      chatServiceFactory.getActiveChatContacts = getActiveChatContacts;
      chatServiceFactory.saveMessage = saveMessage;
      chatServiceFactory.cleanChat = cleanChat;
      chatServiceFactory.getConversation = getConversation;
      chatServiceFactory.markContact = markContact;
      chatServiceFactory.getContactInfo = getContactInfo;
      chatServiceFactory.listMessages = listMessages;
      chatServiceFactory.searchUserInList = searchUserInList;
      chatServiceFactory.markMessagesAsRead = markMessagesAsRead;
      chatServiceFactory.uploadFile = uploadFile;
      chatServiceFactory.sendFile = sendFile;
      chatServiceFactory.fileType = fileType;
      chatServiceFactory.registerContact = registerContact;
      chatServiceFactory.updateContact = updateContact;
      return chatServiceFactory;

  }]);


    /* Este servicio es útil para cuando el usuario de la sucursal se encuentre en otras secciones del sistema */
    app.factory('chatOutTattler', ['$resource', '$cookies', 'AppConfig', '$rootScope', '$log', '$http', '$q', 'chatTattler','localStorageService',function ($resource, $cookies, AppConfig, $rootScope, $log, $http, $q, chatTattler, localStorageService) {
        var chatOutTattlerFactory = {};

        var _initListener = function(){

            if(typeof(QB.chat) === 'undefined' || QB.chat=== null){

                return;
            }
            if(typeof($rootScope.chatListener) !== 'undefined' && $rootScope.chatListener !== null)
            {
                QB.chat.deleteListener($rootScope.chatListener);
                $log.log("deleteListener");
            }
            var bot = chatBot();

            QB.chat.onMessageListener = function(userId, message) {
                // callback function

                if(typeof(userId) != 'undefined' && typeof(message) != 'undefined') {
                    if(_.indexOf($rootScope.contactsList, userId) === -1){
                        $rootScope.contactsList.push(userId);
                    }
                    $log.log("AQUIIIII recibi");

                    if (!("Notification" in window)) {
                        alert("Tu navegador no soporta las notificaciones para el Chat. Puedes usar Firefox o Chrome");
                    }

                    // Let's check if the user is okay to get some notification
                    else if (Notification.permission === "granted") {
                        // If it's okay let's create a notification
                        var notification = new Notification("Tienes un nuevo mensaje en el chat:", {
                            body: message.body,
                            icon: "./components/app-data/img/home/tattler.png"

                        });
                    }

                    // Otherwise, we need to ask the user for permission
                    else if (Notification.permission !== 'denied') {
                        Notification.requestPermission(function (permission) {
                            // If the user is okay, let's create a notification
                            if (permission === "granted") {
                                var notification = new Notification("Tienes un nuevo mensaje en el chat:", {
                                    body: message.body,
                                    icon: "./components/app-data/img/home/tattler.png"

                                });
                            }
                        });
                    }
                    if(bot !== null && bot.messageResponse!== ""){
                        sendAutomaticMessage(bot.messageResponse, userId);
                    }

                    var contacts = chatTattler.getActiveChatContacts();

                    contacts.then(function(response) {
                        // $rootScope.activeConversations = response.length;

                    },function(err){

                    });

                }

            };

            return QB.chat.addListener({name: 'message', type: 'chat', live: true}, QB.chat.onMessageListener);

        };

        var sendAutomaticMessage = function(msg, userid){
            var par = {
                full_name: $rootScope.name,
                lat: '',
                long: ''
            };

            chatTattler.sendMessage(getSenderJid(userid), msg, par, userid, 'Text');
        };

        var getSenderJid = function(senderId){
            return QB.chat.helpers.getUserJid(senderId, $rootScope.app_id);
        };

        var chatBot = function(){
            var bot =  localStorageService.get('chatResponse');
            if (!angular.isUndefinedOrNull(bot)) {
                return bot;
            }
            return null;
        };

        chatOutTattlerFactory.initListener = _initListener;

        return chatOutTattlerFactory;
    }]);

})();
