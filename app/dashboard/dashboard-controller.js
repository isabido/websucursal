(function () {
  'use strict';
  var app = angular.module("tattlerApp");
app.controller("DashboardController", function($scope, $http, $cookieStore, $cookies, auth, $log, AppConfig){

	var route = AppConfig.server + "branch/summary";
	$scope.getConversationId = function(){
        $http({
          method: "get",
          url: route,
          headers: {'Authorization': 'bearer ' + $cookies.token}
        }).
        success(function(data,status) {
          //$scope.conversationId = data.id;
          $scope.minutes = data.minutes_count;
          $scope.cupones = data.claimed_coupons_count;
          $scope.contactos = data.contacts_count;
          $log.log(data);
        }).
        error(function(data,status){
          $log.log("No se pudieron obtener los datos.")
        });

	};

	$scope.getConversationId();

});
})();
