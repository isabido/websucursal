(function () {
  'use strict';
  var app = angular.module("tattlerApp");
  app.controller("PromocionesController", ['$scope', '$http', '$modal', '$log', 'promotionsFactory', function($scope, $http, $modal, $log, promotionsFactory){

    $scope.originalUser= angular.copy($scope.user);
    var route = $scope.$root.server + "branch/promotions";
    $scope.promotions = {};

    $scope.openModal = function (size, data)
    {
        $log.log(data);
        var modalInstance = $modal.open({
          templateUrl: 'promociones/views/promocion.html',
          controller: 'myModalController',
          size: size,
          resolve: {
            Item: function() //scope del modal
          {
            return data;
          }
        }
      });
    };

    //$scope.promotions = getPromotions();
    
    $scope.getPromotions = function(){
        var get = promotionsFactory.getPromotions();
        get.then(function (response) {
            $scope.promotions = response;
        }, function (err){
            $log.log(err);
        });
    };

    $scope.date = function(isoDate) {

        var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
        var f=new Date();
        return diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear();
    };

  }]);

  app.controller('myModalController', ['$scope','$modalInstance','Item', '$cookies', '$log', '$http', 'promotionFactory', function($scope, $modalInstance,Item, $cookies, $log, $http, promotionFactory)
{
    $scope.promotion = Item;
    $log.log(Item);
    var route = $scope.$root.server + "branch/promotions/"+$scope.promotion;
    var routePromotion = $scope.$root.server + "company/promotions/"+$scope.promotion+"/coupons/";
    $log.log(route);
    $scope.alerts = [];

    $scope.addAlert = function() {

    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

  $scope.getPromotion = function(){

      var info = promotionFactory.getInfoPromotion($scope.promotion);
      info.then(function(data){
              $scope.image = data.image;
              $scope.branches = data.branches;
              $scope.promo = data.title;
              $scope.startDate = data.start_date
              $scope.expirationDate = data.expiration_date;
              $scope.cantidadCup = data.issued_coupons;
          },
          function(err){
              $log.log(err);
          });

    };


    $scope.getPromotion();
    $scope.putCoupon = function(coupon){

    };
  $scope.saveCoupon = function (coupon)
  {
      $scope.alerts = [];
    $log.log(coupon);
    var put = promotionFactory.setCoupon($scope.promotion, coupon);
      put.then(function(response){
          if(response.was_succesful){
              $scope.alerts.push({type: 'success', msg: 'Se ha registrado el cupón.'});
              //$modalInstance.cancel("Se ha agregado el cupón.");
          }else{
              $scope.alerts.push({type: 'danger', msg: 'No se ha podido agregar el cupón.'});
              $log.log("No Se ha podido guardar el cupon");
          }
      }, function(err){
          $scope.alerts.push({type: 'danger', msg: err.Message});
          $log.log("No Se ha podido guardar el cupon");
      })
    
  };

  $scope.cancel = function ()
  {
    $modalInstance.dismiss('cancel');
  };

  $scope.postPromotion = function(){
    $http({
          method: "put",
          url: routePromotion,
          headers: {'Content-Type': 'application/json'},
          data: $.param({promotion_id: userDetails.email, coupon_code: userDetails.password})
      }).
        success(function(data,status) {
          
              var chat_token = JSON.parse(data.chat_session);
              //$window.alert(chat_token.token);
              $log.log(chat_token.token);
              var params = {email: userDetails.email, password: userDetails.password};
              

            auth.login(data.username, data.access_token, chat_token.token, params);
            $scope.resetUserForm(true);

            //auth.QBInit(chat_token.token, params);
            //$scope.$root.showMenu=true;
      }).
        error(function(data,status){
            $scope.resetUserForm(false);
        });
  }

}]);

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/promociones", {
                templateUrl: "promociones/promociones.html",
                css: "promociones/promociones.css",
                controller: "PromocionesController"
            });
    });
})();
