(function () {
  'use strict';
 var app = angular.module("tattlerApp");
  app.controller("callsController",['$scope', '$modal', '$log', 'callsFactory', '$http', '$cookies', function($scope, $modal, $log, callsFactory, $http, $cookies){

  	var route = $scope.$root.server + "branch/promotions";
  	var routeCalls = $scope.$root.server + "branch/calls";

      $scope.customer = [];


      $scope.getCalls = function(){
          var get = callsFactory.getBranchCalls();
          get.then(function(response){

              $log.log(response);
              $scope.customer = response.customer;
          }, function(err){


          });
      };
    $scope.openModal = function (data)
    {
        $log.log(data);
        var modalInstance = $modal.open({
          templateUrl: 'reporte/reporte.html',
          controller: 'myModalControllerReporte',
          resolve: {
            Item: function() //scope del modal
          {
            return data;
          }
        }
      });
    };




    $scope.getCalls();

  }]);

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/calls", {
                templateUrl: "llamadas/llamadas.html",
                css: "llamadas/llamadas.css",
                controller: "callsController"
            });
    });

})();
