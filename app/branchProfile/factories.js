/**
 * Created by isabido on 20/02/2015.
 */
(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('profileBranch', ['$resource', '$cookies', 'AppConfig', '$rootScope', '$log', '$http', '$q',function ($resource, $cookies, AppConfig, $rootScope, $log, $http, $q) {
        var profileBranch = {};
        var serviceBase = AppConfig.server;
        var ruta = serviceBase + "branch";
        var getProfileBranch = function(){
            var deferred=$q.defer();

            $http.get(ruta).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });


            return deferred.promise;
        };

        profileBranch.getProfileBranch = getProfileBranch;
        return profileBranch;

    }]);

})();